[![Author](https://img.shields.io/badge/author-Abu_Ayyub-lightgrey.svg)](https://bitbucket.com/luthfie)
[![License](https://img.shields.io/badge/license-Apache2.0-lightblue.svg)](https://bitbucket.com/luthfie/testbitbucket/src/master/license.txt)


# Testing Script
- This is only a testing script for AI.


# Installation
- For AI users, simply use command:

```
$ ai pkg addrepository luthfie/testbitbucket
$ ai install ext.testbitbucket
```


# Testing Check
- For AI users, sample commands:

```
$ ai testbitbucket check <path/to/file>
```

- Use ```$ ai testbitbucket help``` for more detail.


